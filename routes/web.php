<?php


// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('cars','CarsController');

    Route::resource('fees','FeesController');

    Route::get('reviews',['uses'=>'ReviewsController@index','as'=>'reviews.index']);
    Route::get('reviews/create',['uses'=>'ReviewsController@create','as'=>'reviews.create']);
    Route::post('reviews/store',['uses'=>'ReviewsController@store','as'=>'reviews.store']);
    Route::get('reviews/{id}/edit',['uses'=>'ReviewsController@edit','as'=>'reviews.edit']);
    Route::put('reviews/{id}/update',['uses'=>'ReviewsController@update','as'=>'reviews.update']);

    Route::get('reviews/{review_id}/submissions',['uses'=>'ReviewsSubmissionController@index','as'=>'review-submission.index']);
    Route::get('reviews/{review_id}/submissions/add',['uses'=>'ReviewSubmissionController@create','as'=>'review-submission.create']);
    Route::post('reviews/{review_id}/submissions/store',['uses'=>'ReviewSubmissionController@store','as'=>'review-submission.store']);
    Route::get('reviews/{review_id}/submissions/{id}/view',['uses'=>'ReviewSubmissionController@view','as'=>'review-submission.view']);

    Route::get('car-schedule',['uses'=>'CarInstructorScheduleController@index','as'=>'car-instructor-schedule.index']);
    Route::get('car-schedule/create',['uses'=>'CarInstructorScheduleController@create','as'=>'car-instructor-schedule.create']);
    Route::post('car-schedule/store',['uses'=>'CarInstructorScheduleController@store','as'=>'car-instructor-schedule.store']);
    Route::get('car-schedule/{id}/edit',['uses'=>'CarInstructorScheduleController@edit','as'=>'car-instructor-schedule.edit']);
    Route::put('car-schedule/{id}/update',['uses'=>'CarInstructorScheduleController@update','as'=>'car-instructor-schedule.update']);
    Route::delete('car-schedule/{id}/destroy',['uses'=>'CarInstructorScheduleController@destroy','as'=>'car-instructor-schedule.destroy']);
    Route::get('car-schedule/{car_id}',['uses'=>'CarInstructorScheduleController@show','as'=>'car-instructor-schedule.show']);

    Route::get('fees/{fee_id}/installments',['uses'=>'FeeInstallmentsController@index','as'=>'fee-installments.index']);
    Route::get('fees/{fee_id}/installments/add',['uses'=>'FeeInstallmentsController@create','as'=>'fee-installments.create']);
    Route::post('fees/{fee_id}/installments/store',['uses'=>'FeeInstallmentsController@store','as'=>'fee-installments.store']);
    Route::get('fees/{fee_id}/installments/{id}/edit',['uses'=>'FeeInstallmentsController@edit','as'=>'fee-installments.edit']);
    Route::put('fees/{fee_id}/installments/{id}/update',['uses'=>'FeeInstallmentsController@update','as'=>'fee-installments.update']);
    Route::delete('fees/{fee_id}/installments/{id}/delete',['uses'=>'FeeInstallmentsController@destroy','as'=>'fee-installments.destroy']);
    Route::get('fees/{fee_id}/installments/{id}/show',['uses'=>'FeeInstallmentsController@show','as'=>'fee-installments.show']);
    Route::get('fees/{fee_id}/installments/{id}/pay',['uses'=>'PaymentsController@order','as'=>'payments.pay']);
    Route::post('payment/status',['uses'=>'PaymentsController@paymentCallback','as'=>'payments.save']);

    Route::get('client-schedule/{schedule_id}/book_appointment',['uses'=>'ClientScheduleController@book_appointment','as'=>'client-schedule.book_appointment']);
    Route::get('client-schedule',['uses'=>'ClientScheduleController@index','as'=>'client-schedule.index']);
    Route::get('client-schedule/{id}/instructor_login',['uses'=>'ClientScheduleController@instructor_login','as'=>'client-schedule.instructor_login']);
    Route::get('client-schedule/{id}/student_login',['uses'=>'ClientScheduleController@student_login','as'=>'client-schedule.student_login']);
    Route::get('client-schedule/{id}/student_logout',['uses'=>'ClientScheduleController@student_logout','as'=>'client-schedule.student_logout']);
    Route::get('client-schedule/{id}/instructor_logout',['uses'=>'ClientScheduleController@instructor_logout','as'=>'client-schedule.instructor_logout']);
    
    Route::get('client-schedule/{id}/show',['uses'=>'ClientScheduleController@show','as'=>'client-schedule.show']);
    // Dashboard
    Route::get('/', ['uses'=>'HomeController@index','as'=>'dashboard']);

    // Language
    Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

    // Users
    Route::get('users/import', ['uses'=>'UsersController@getImport','as'=>'users.import']);
    Route::post('users/import_parse', ['uses'=>'UsersController@parseImport','as'=>'users.import_fields']);
    Route::post('users/import_process', ['uses'=>'UsersController@processImport','as'=>'users.import_process']);
    Route::get('users',['uses'=>'UsersController@index','as'=>'users.index']);
    Route::get('users/add',['uses'=>'UsersController@create','as'=>'users.create']);
    Route::post('users/store',['uses'=>'UsersController@store','as'=>'users.store']);
    Route::get('users/{id}/edit',['uses'=>'UsersController@edit','as'=>'users.edit']);
    Route::put('users/{id}/update',['uses'=>'UsersController@update','as'=>'users.update']);
    Route::delete('users/{id}/destroy',['uses'=>'UsersController@destroy','as'=>'users.destroy']);
    Route::get('users/{id}',['uses'=>'UsersController@show','as'=>'users.show']);

    Route::get('user-actions', ['uses'=>'UserActionsController@index','as'=>'user_actions.index']);
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');
});
