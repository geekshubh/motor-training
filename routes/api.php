<?php

Route::post('user/login', 'API\LoginController@login');
//  Route::post('logout', 'AuthController@logout');
//  Route::post('refresh', 'AuthController@refresh');

Route::group(['middleware'=>['jwt.verify']],function(){
  // Fees Index
  Route::get('fees',['uses'=>'FeesController@index','as'=>'fees-api.index']);
  // Fees Create
  Route::get('fees/create',['uses'=>'FeesController@create','as'=>'fees-api.create']);
  // Fees Store
  Route::post('fees/store',['uses'=>'FeesController@store','as'=>'fees-api.store']);
  // Fees Edit
  Route::get('fees/{id}/edit',['uses'=>'FeesController@edit','as'=>'fees-api.edit']);
  // Fees Update
  Route::put('fees/{id}/update',['uses'=>'FeesController@update','as'=>'fees-api.update']);
  // Fees Delete
  Route::delete('fees/{id}/destroy',['uses'=>'FeesController@destroy','as'=>'fees-api.destroy']);
  // Fees Show
  Route::get('fees/{id}',['uses'=>'FeesController@show','as'=>'fees-api.show']);

  // Payments
  Route::get('fees/{id}/payment',['uses'=>'PaymentsController@pay','as'=>'payments-api.pay']);
  // Payments Post
  Route::post('fees/pay',['uses'=>'PaymentsController@payment','as'=>'payments-api.store']);


  // Home Route
  Route::get('/', ['uses'=>'HomeController@index','as'=>'dashboard-api']);

  // Invoice Index Route
  Route::get('invoices',['uses'=>'InvoicesController@index','as'=>'invoices-api.index']);
  // Invoice Create Route
  Route::get('invoices/create',['uses'=>'InvoicesController@create','as'=>'invoices-api.create']);
  // Invoice Store Route
  Route::post('invoices/store',['uses'=>'InvoicesController@store','as'=>'invoices-api.store']);
  // Invoice Edit Route
  Route::get('invoices/{id}/edit',['uses'=>'InvoicesController@edit','as'=>'invoices-api.edit']);
  // Invoice Update Route
  Route::put('invoices/{id}/update',['uses'=>'InvoicesController@update','as'=>'invoices-api.update']);
  // Invoice Delete Route
  Route::delete('invoices/{id}/destroy',['uses'=>'InvoicesController@destroy','as'=>'invoices-api.destroy']);
  // Invoice Show Route
  Route::get('invoices/{id}',['uses'=>'InvoicesController@show','as'=>'invoices-api.show']);
  // Invoice PDF Route
  // Route::get('invoices/{id}/pdf',['uses'=>'InvoicesController@pdf','as'=>'invoices.pdf']);

  // Invoice Item Index Route
  Route::get('invoices/{invoice_id}/items',['uses'=>'InvoiceItemsController@index','as'=>'invoice-items-api.index']);
  // Invoice Item Create Route
  Route::get('invoices/{invoice_id}/items/add',['uses'=>'InvoiceItemsController@create','as'=>'invoice-items-api.create']);
  // Invoice Item Store Route
  Route::post('invoices/{invoice_id}/items/store',['uses'=>'InvoiceItemsController@store','as'=>'invoice-items-api.store']);
  // Invoice Item Edit Route
  Route::get('invoices/{invoice_id}/items/{id}/edit',['uses'=>'InvoiceItemsController@edit','as'=>'invoice-items-api.edit']);
  // Invoice Item Update Route
  Route::put('invoices/{invoice_id}/items/{id}/update',['uses'=>'InvoiceItemsController@update','as'=>'invoice-items-api.update']);
  // Invoice Item Delete Route
  Route::delete('invoices/{invoice_id}/items/{id}/destroy',['uses'=>'InvoiceItemsController@destroy','as'=>'invoice-items-api.destroy']);
  // Invoice Item Show Route
  Route::get('invoices/{invoice_id}/items/{id}',['uses'=>'InvoiceItemsController@show','as'=>'invoice-items-api.show']);


  // Materials Index Route
  Route::get('materials',['uses'=>'EducationalMaterialsController@index','as'=>'materials-api.index']);
  // Materials Add Route
  Route::get('materials/add',['uses'=>'EducationalMaterialsController@create','as'=>'materials-api.create']);
  // Materials Store Route
  Route::post('materials/store',['uses'=>'EducationalMaterialsController@store','as'=>'materials-api.store']);
  // Materials Edit Route
  Route::get('materials/{id}/edit',['uses'=>'EducationalMaterialsController@edit','as'=>'materials-api.edit']);
  // Materials Update Route
  Route::put('materials/{id}/update',['uses'=>'EducationalMaterialsController@update','as'=>'materials-api.update']);
  // Materials Delete Route
  Route::delete('materials/{id}/destroy',['uses'=>'EducationalMaterialsController@destroy','as'=>'materials-api.destroy']);

  // News Index Route
  Route::get('news',['uses'=>'ImportantNotificationsController@index','as'=>'news-api.index']);
  // Add News Route
  Route::get('news/add',['uses'=>'ImportantNotificationsController@create','as'=>'news-api.create']);
  // Store News Route
  Route::post('news/store',['uses'=>'ImportantNotificationsController@store','as'=>'news-api.store']);
  // Edit News Route
  Route::get('news/{id}/edit',['uses'=>'ImportantNotificationsController@edit','as'=>'news-api.edit']);
  // Update News Route
  Route::put('news/{id}/update',['uses'=>'ImportantNotificationsController@update','as'=>'news-api.update']);
  // View News Route
  Route::get('news/{id}',['uses'=>'ImportantNotificationsController@show','as'=>'news-api.show']);
  // Delete News Route
  Route::delete('news/{id}/destroy',['uses'=>'ImportantNotificationsController@destroy','as'=>'news-api.destroy']);

  // Online Exams Index Route
  Route::get('online-exams',['uses'=>'OnlineExamsController@index', 'as'=>'online-exams-api.index']);
  // Online Exams Add Route
  Route::get('online-exams/add',['uses'=>'OnlineExamsController@create','as'=>'online-exams-api.create']);
  // Online Exams Store Route
  Route::post('online-exams/store',['uses'=>'OnlineExamsController@store','as'=>'online-exams-api.store']);
  // Online Exams Edit Route
  Route::get('online-exams/{id}/edit', ['uses'=>'OnlineExamsController@edit','as'=>'online-exams-api.edit']);
  // Online Exams Update Route
  Route::put('online-exams/{id}/update',['uses'=>'OnlineExamsController@update','as'=>'online-exams-api.update']);
  // Online Exams Individual Exam Route
  Route::get('online-exams/{id}',['uses'=>'OnlineExamsController@take_exams','as'=>'online-exams-api.take_exams']);
  // Online Exams Individual Exam store Route
  Route::post('online-exams/{online_exam_id}/store-answer',['uses'=>'OnlineExamsController@store_submissions','as'=>'online-exams-api.store_submissions']);
  // Online Exams Questions
  Route::get('online-exams/{online_exam_id}/questions',['uses'=>'OnlineExamsController@questions','as'=>'online-exams-api.questions']);
  // Online Exam Question Options
  Route::get('online-exams/{online_exam_id}/questions/{question_id}/options',['uses'=>'OnlineExamsController@questions_options','as'=>'online-exams-api.questions_options']);
  // Online Exam Question Delete
  Route::delete('online-exams/{online_exam_id}/question/{id}/delete',['uses'=>'OnlineExamsController@destroy_questions','as'=>'online-exams-api.destroy_questions']);
  // Online Exam Question Add Route
  Route::get('online-exams/{online_exam_id}/question/add',['uses'=>'OnlineExamsController@add_questions','as'=>'online-exams-api.add_question']);
  // Online Exam Store Individual Question Route
  Route::post('online-exams/question/store/{online_exam_id}',['uses'=>'OnlineExamsController@store_questions','as'=>'online-exams-api.store_question']);

  // Profile Route
  Route::get('profile', ['uses'=>'ProfileController@index','as'=> 'users-api.profile']);
  // Profile Edit Route
  Route::get('profile/{id}/edit',['uses'=>'ProfileController@edit', 'as'=>'users-api.edit_profile']);
  // Profile Update Route
  Route::put('profile/update/{id}',['uses'=>'ProfileController@update', 'as'=>'users-api.update_profile']);

  // Questions Import Route
  Route::get('questions/import', ['uses'=>'QuestionsController@getImport','as'=>'questions-api.import']);
  Route::post('questions/import_parse', ['uses'=>'QuestionsController@parseImport','as'=>'questions-api.import_fields']);
  Route::post('questions/import_process', ['uses'=>'QuestionsController@processImport','as'=>'questions-api.import_process']);

  // Questions Index Route
  Route::get('questions',['uses'=>'QuestionsController@index','as'=>'questions-api.index']);
  // Add Question Route
  Route::get('questions/add',['uses'=>'QuestionsController@create','as'=>'questions-api.create']);
  // Store Question Route
  Route::post('questions/store',['uses'=>'QuestionsController@store','as'=>'questions-api.store']);
  // Edit Question Route
  Route::get('questions/{id}/edit',['uses'=>'QuestionsController@edit','as'=>'questions-api.edit']);
  // Update Question Route
  Route::put('questions/{id}/update',['uses'=>'QuestionsController@update','as'=>'questions-api.update']);
  // View Question Route
  Route::get('questions/{id}',['uses'=>'QuestionsController@show','as'=>'questions-api.show']);
  // Delete Question Route
  Route::delete('questions/{id}/destroy',['uses'=>'QuestionsController@destroy','as'=>'questions-api.destroy']);
  // View Question Option Route
  Route::get('questions/{question_id}/options',['uses'=>'QuestionsController@show_options','as'=>'questions-api.show_options']);
  // Delete Question Option Route
  Route::delete('questions/{question_id}/options/{id}/delete',['uses'=>'QuestionsController@destroy_options','as'=>'questions-api.destroy_options']);

  // Question Options Route
  Route::get('questions-options/import', ['uses'=>'QuestionsOptionsController@getImport','as'=>'questions_options-api.import']);
  Route::post('questions-options/import_parse', ['uses'=>'QuestionsOptionsController@parseImport','as'=>'questions_options-api.import_fields']);
  Route::post('questions-options/import_process', ['uses'=>'QuestionsOptionsController@processImport','as'=>'questions_options-api.import_process']);

  // Question Options Index Route
  Route::get('questions-options',['uses'=>'QuestionsOptionsController@index','as'=>'questions_options-api.index']);
  // Add Question Option Route
  Route::get('questions-options/add',['uses'=>'QuestionsOptionsController@create','as'=>'questions_options-api.create']);
  // Store Question Option Route
  Route::post('questions-options/store',['uses'=>'QuestionsOptionsController@store','as'=>'questions_options-api.store']);
  // Edit Question Option Route
  Route::get('questions-options/{id}/edit',['uses'=>'QuestionsOptionsController@edit','as'=>'questions_options-api.edit']);
  // Update Question Option Route
  Route::put('questions-options/{id}/update',['uses'=>'QuestionsOptionsController@update','as'=>'questions_options-api.update']);
  // Delete Question Option Route
  Route::delete('questions-options/{id}/destroy',['uses'=>'QuestionsOptionsController@destroy','as'=>'questions_options-api.destroy']);

  // Results Index Route
  Route::get('results',['uses'=>'ResultsController@index','as'=>'results-api.index']);
  // Results Show Route
  Route::get('results/{id}/view-result',['uses'=>'ResultsController@show','as'=>'results-api.show']);

  // Student Parents Import Route
  Route::get('users/import', ['uses'=>'UsersController@getImport','as'=>'users-api.import']);
  Route::post('users/import_parse', ['uses'=>'UsersController@parseImport','as'=>'users-api.import_fields']);
  Route::post('users/import_process', ['uses'=>'UsersController@processImport','as'=>'users-api.import_process']);
  // Users Index Route
  Route::get('users',['uses'=>'UsersController@index','as'=>'users-api.index']);
  // Add User Route
  Route::get('users/add',['uses'=>'UsersController@create','as'=>'users-api.create']);
  // Store User Route
  Route::post('users/store',['uses'=>'UsersController@store','as'=>'users-api.store']);
  // Edit User Route
  Route::get('users/{id}/edit',['uses'=>'UsersController@edit','as'=>'users-api.edit']);
  // Update User Route
  Route::put('users/{id}/update',['uses'=>'UsersController@update','as'=>'users-api.update']);
  // Delete User Route
  // View User Route
  Route::delete('users/{id}/destroy',['uses'=>'UsersController@destroy','as'=>'users-api.destroy']);
  Route::get('users/{id}',['uses'=>'UsersController@show','as'=>'users-api.show']);

});
