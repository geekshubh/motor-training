@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>View Schedule Details</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('client-schedule.index')}}">Schedule</a></li>
              <li class="active">View Schedule Details</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">View Schedule Details
            <a href="{{ route('client-schedule.index')}}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <div class="row">
              <div class ="col-sm-12 col-md-12 col-lg-12">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Car</td>
                      <td>{{ $schedule->client_schedule_car->name }}</td>
                    </tr>
                    <tr>
                      <td>Instructor</td>
                      <td>{{ $schedule->instructor_schedule->name }}</td>
                    </tr>
                    <tr>
                      <td>Client</td>
                      <td>{{ $schedule->client_schedule->name }}</td>
                    </tr>
                    <tr>
                      <td>Date</td>
                      <td>{{ $schedule->date }}</td>
                    </tr>
                    <tr>
                      <td>Start Time</td>
                      <td>{{ $schedule->start_time }}</td>
                    </tr>
                    <tr>
                      <td>End Time</td>
                      <td>{{ $schedule->end_time }}</td>
                    </tr>
                    @if(!empty($schedule->instructor_login_time))
                    <tr>
                      <td>Instructor Login Time</td>
                      <td>{{ $schedule->instructor_login_time }}</td>
                    </tr>
                    @endif
                    @if((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)))
                    <tr>
                      <td>Student Login Time</td>
                      <td>{{ $schedule->client_login_time }}</td>
                    </tr>
                    @endif
                    @if((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)) && (!empty($schedule->client_logout_time)))
                    <tr>
                      <td>Student Logout Time</td>
                      <td>{{ $schedule->client_logout_time }}</td>
                    </tr>
                    @endif

                    @if((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)) && (!empty($schedule->client_logout_time)) && (!empty($schedule->instructor_logout_time)))
                    <tr>
                      <td>Instructor Logout Time</td>
                      <td>{{ $schedule->instructor_logout_time }}</td>
                    </tr>
                    @endif

                    @if(!empty($schedule->instructor_total_time))
                    <tr>
                      <td>Instructor Total Time</td>
                      <td>{{ $schedule->instructor_total_time }}</td>
                    </tr>
                    @endif

                    @if(!empty($schedule->client_total_time))
                    <tr>
                      <td>Student Total Time</td>
                      <td>{{ $schedule->client_total_time }}</td>
                    </tr>
                    @endif

                    @if(!empty($schedule->average_time))
                    <tr>
                      <td>Average Time</td>
                      <td>{{ $schedule->average_time }}</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@stop
