@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Client Schedule</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li class="active">Client Schedule</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Client Schedule</strong>
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  @if(Auth::user()->isAdmin())
                  <th>Car Name</th>
                  <th>Instructor Name</th>
                  <th>Client Name</th>
                  @endif
                  <th>Date</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($schedule) > 0)
                @foreach ($schedule as $schedule)
                <tr>
                  @if(Auth::user()->isAdmin())
                  <td>{{ $schedule->client_schedule_car->name }}</td>
                  <td>{{ $schedule->instructor_schedule->name }}</td>
                  <td>{{ $schedule->client_schedule->name }}</td>
                  @endif
                  <td>{{ $schedule->date }}</td>
                  <td>{{ $schedule->start_time }}</td>
                  <td>{{ $schedule->end_time }}</td>
                  <td>
                    @if((Auth::user()->isInstructor() || Auth::user()->isAdmin()) && (empty($schedule->instructor_login_time)))
                      <a href="{{ route('client-schedule.instructor_login',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Instructor Login</a>
                    @endif

                    @if((Auth::user()->isStudent()) && ((!empty($schedule->instructor_login_time)) && (empty($schedule->client_login_time))))
                      <a href="{{ route('client-schedule.student_login',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Student Login</a>
                    @endif

                    @if(((Auth::user()->isInstructor()) || Auth::user()->isAdmin()) && ((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)) && (!empty($schedule->client_logout_time)) && (empty($schedule->instructor_logout_time))))
                      <a href="{{ route('client-schedule.instructor_logout',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info"> Instructor Logout</a>
                    @endif

                    @if((Auth::user()->isStudent()) && ((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)) && (empty($schedule->client_logout_time))))
                      <a href="{{ route('client-schedule.student_logout',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Student Logout</a>
                    @endif
                    <a href="{{ route('client-schedule.show',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Show Schedule</a>
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
