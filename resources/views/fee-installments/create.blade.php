@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Add Fee Installment</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('fee-installments.index',[$fee_id])}}">Fee Installments</a></li>
              <li class="active">Add Fee Installment</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Add Fee Installment
            <a href="{{ route('fee-installments.index',[$fee_id])}}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::open(['method' => 'POST', 'route' => ['fee-installments.store',$fee_id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Title</h6>
              {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Amount</h6>
              {!! Form::text('amount', old('amount'), ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Status</h6>
              {!! Form::select('status', $status, old('status'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Payment Type</h6>
              {!! Form::select('payment_type', $type, old('payment_type'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
