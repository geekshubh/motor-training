@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Fee Installments</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li class="active">Fee Installments</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Fee Installments</strong>
            <a href="{{ route('fee-installments.create',[$fee_id])}}" class="btn btn-md btn-success float-right">Add Fee Installment</a>
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($installments) > 0)
                @foreach ($installments as $installment)
                <tr>
                  <td>{{ $installment->title }}</td>
                  <td>
                    @if(Auth::user()->isAdmin())
                    <a href="{{ route('fee-installments.edit',['id'=>$installment->id,'fee_id'=>$installment->fee_id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                    {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['fee-installments.destroy', $installment->fee_id,$installment->id])) !!}
                    {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                    {!! Form::close() !!}
                    @endif
                    <a href="{{ route('fee-installments.show',['id'=>$installment->id,'fee_id'=>$installment->fee_id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">View</a>
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
