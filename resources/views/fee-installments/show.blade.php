@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>View Fee Installment</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('fee-installments.index',[$fee_id])}}">Fee Installments</a></li>
              <li class="active">View Fee Installment</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">View Fee Installment
            <a href="{{ route('fee-installments.index',[$fee_id]) }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <div class="row">
              <div class ="col-sm-12 col-md-12 col-lg-12">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Unique ID</td>
                      <td>{{ $installment->installment_unique_id }}</td>
                    </tr>
                    <tr>
                      <td>Title</td>
                      <td>{{ $installment->title }}</td>
                    </tr>
                    <tr>
                      <td>Client</td>
                      <td>{{ $installment->fee_installments->name }}</td>
                    </tr>
                    <tr>
                      <td>Amount</td>
                      <td>{{ $installment->amount }}</td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td>{{ $installment->status }}</td>
                    </tr>
                    <tr>
                      <td>Payment Type</td>
                      <td>{{ $installment->payment_type }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@stop
