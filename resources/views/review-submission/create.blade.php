@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Add Review</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('reviews.index')}}">Reviews</a></li>
              <li class="active">Add Review</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Add Review
            <a href="{{ route('reviews.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::open(['method' => 'POST', 'route' => ['review-submission.store',$review_id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>How satisfied are you with the progress?</h6>
              <input type="hidden" name="questions[1]" value="How satisfied are you with the progress?">
              <label class="radio-inline">
                <input type="radio" name="answers[1]" value="1">1 (Very Poor)
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[1]" value="2">2
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[1]" value="3">3
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[1]" value="4">4
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[1]" value="5">5 (Excellent)
              </label>
            </div>
            <br>
            <div class="form-group">
              <h6>Does the trainer address your queries?</h6>
              <input type="hidden" name="questions[2]" value="How satisfied are you with the progress?">
              <label class="radio-inline">
                <input type="radio" name="answers[2]" value="1">1 (Not at all)
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[2]" value="2">2
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[2]" value="3">3
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[2]" value="4">4
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[2]" value="5">5 (To Full Satisfaction)
              </label>
            </div>
            <br>
            <div class="form-group">
              <h6>How would you rate trainer's knowledge</h6>
              <input type="hidden" name="questions[3]" value="How satisfied are you with the progress?">
              <label class="radio-inline">
                <input type="radio" name="answers[3]" value="1">1 (Very Poor)
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[3]" value="2">2
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[3]" value="3">3
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[3]" value="4">4
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[3]" value="5">5 (Excellent)
              </label>
            </div>
            <br>
            <div class="form-group">
              <h6>How would you rate his skills (Training & Driving)</h6>
              <input type="hidden" name="questions[4]" value="How would you rate his skills (Training & Driving)">
              <label class="radio-inline">
                <input type="radio" name="answers[4]" value="1">1 (Very Poor)
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[4]" value="2">2
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[4]" value="3">3
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[4]" value="4">4
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[4]" value="5">5 (Excellent)
              </label>
            </div>
            <br>
            <div class="form-group">
              <h6>How do you find his behaviour & etiquettes?</h6>
              <input type="hidden" name="questions[5]" value="How do you find his behaviour & etiquettes?">
              <label class="radio-inline">
                <input type="radio" name="answers[5]" value="1">1 (Very Poor)
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[5]" value="2">2
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[5]" value="3">3
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[5]" value="4">4
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[5]" value="5">5 (Excellent)
              </label>
            </div>
            <br>
            <div class="form-group">
              <h6>Has the trainer ever asked for any favour?(money, gifts, dakshina etc)</h6>
              <input type="hidden" name="questions[6]" value="Has the trainer ever asked for any favour?(money, gifts, dakshina etc)">
              <input type = "text" name = "answers[6]" class="form-control">
            </div>
            <br>
            <div class="form-group">
              <h6>Any particular thing you would like to bring into our notice</h6>
              <input type="hidden" name="questions[7]" value="Has the trainer ever asked for any favour?(money, gifts, dakshina etc)">
              <input type = "text" name = "answers[7]" class="form-control">
            </div>
            <br>
            @if($review_id != 1)
            <div class="form-group">
              <h6>Can you see the desired changes from your past feedback?</h6>
              <input type="hidden" name="questions[8]" value="Can you see the desired changes from your past feedback?">
              <input type = "text" name = "answers[8]" class="form-control">
            </div>
            <br>
            @endif
            <div class="form-group">
              <h6>Overall feedback and your impression about our service</h6>
              <input type="hidden" name="questions[9]" value="Overall feedback and your impression about our service">
              <input type = "text" name = "answers[9]" class="form-control">
            </div>
            <br>
            <div class="form-group">
              <h6>Would you recommend us to your friends/ relatives/ acquaintances</h6>
              <input type="hidden" name="questions[10]" value="Would you recommend us to your friends/ relatives/ acquaintances">
              <label class="radio-inline">
                <input type="radio" name="answers[10]" value="Yes">Yes
              </label>
              <label class="radio-inline">
                <input type="radio" name="answers[10]" value="No">No
              </label>
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
