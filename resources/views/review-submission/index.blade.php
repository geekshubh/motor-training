@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Reviews</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('reviews.index')}}">Reviews</a></li>
              <li class="active">Review Submission</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Reviews</strong>
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($review_submissions) > 0)
                @foreach ($review_submissions as $review_submissions)
                <tr>
                  <td>{{ $review_submissions->submission->name }}</td>
                  <td>
                    <a href="{{ route('review-submission.show',[$review_submissions->id,$review_submissions->review_id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
