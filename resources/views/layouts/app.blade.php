<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.head')
</head>
<body>
    @include('partials.sidebar')
    <div id="right-panel" class="right-panel">
      @include('partials.header')
      @yield('content')
    </div>
</body>
</html>
