<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.auth_head')
</head>
<body class="bg-dark">
  @yield('content')
  @include('partials.auth_javascripts')
</body>
</html>
