@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>All Users</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li class="active">Users</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title"></strong>
            <a href="{{ route('car-instructor-schedule.index')}}" class="btn btn-md btn-danger float-right">Back to List</a>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($schedule) > 0)
                @foreach ($schedule as $schedule)
                <tr>
                  <td>{{ $schedule->start_time }}</td>
                  <td>{{ $schedule->end_time}}</td>
                  <td>
                    @if(Auth::user()->isStudent())
                      @if(empty($schedule->booked_by))
                      <a href="{{ route('client-schedule.book_appointment',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">Book an Appointment</a>
                      @else
                        Slot Already in Use
                      @endif
                    @endif
                    @if(Auth::user()->isAdmin())
                      <a href="{{ route('car-instructor-schedule.edit',[$schedule->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['car-instructor-schedule.destroy', $schedule->id])) !!}
                      {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                    @endif
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- .animated -->
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
