@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Edit Schedule</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('car-instructor-schedule.index')}}">Car & Instructor Schedule</a></li>
              <li class="active">Edit Schedule</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Edit Schedule
            <a href="{{ route('car-instructor-schedule.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::model($schedule,['method' => 'PUT', 'route' => ['car-instructor-schedule.update',$schedule->id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Select Car</h6>
              {!! Form::select('car_id', $cars, old('car_id'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Start Time</h6>
              <div class="input-group date" id="car-schedule-create-start-time" data-target-input="nearest">
                {!! Form::text('start_time',old('start_time'),['class' =>'form-control datetimepicker-input','data-target'=>'#car-schedule-create-start-time']) !!}
                <div class="input-group-append" data-target="#car-schedule-create-start-time" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <br>
            <div class="form-group">
              <h6>End Time</h6>
              <div class="input-group date" id="car-schedule-create-end-time" data-target-input="nearest">
                {!! Form::text('end_time',old('end_time'),['class' =>'form-control datetimepicker-input','data-target'=>'#car-schedule-create-end-time']) !!}
                <div class="input-group-append" data-target="#car-schedule-create-end-time" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <br>
            <div class="form-group">
              <h6>Select Availability</h6>
              {!! Form::select('availability', $availability, old('availability'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
