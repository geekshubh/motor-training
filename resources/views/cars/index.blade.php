@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>All Vehicles</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li class="active">Vehicles</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Vehicles</strong>
            <a href="{{ route('cars.create')}}" class="btn btn-md btn-success float-right">Add Vehicle</a>
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($cars) > 0)
                @foreach ($cars as $car)
                <tr>
                  <td>{{ $car->name }}</td>
                  <td>
                    <a href="{{ route('cars.edit',[$car->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                    {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['cars.destroy', $car->id])) !!}
                    {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                    {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
