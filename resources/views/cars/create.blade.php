@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Add Vehicle</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('cars.index')}}">Vehicle</a></li>
              <li class="active">Add Vehicle</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Add Vehicle
            <a href="{{ route('cars.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::open(['method' => 'POST', 'route' => ['cars.store'] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Vehicle Name</h6>
              {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('name'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Select Instructor</h6>
              {!! Form::select('instructor_id', $instructors, old('instructor_id'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
              @if($errors->has('unavailable_on'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('unavailable_on') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Unavailable On</h6>
              {!! Form::select('unavailable_on', $days, old('unavailable_on'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
              @if($errors->has('unavailable_on'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('unavailable_on') }}</strong>
              </div>
              @endif
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
