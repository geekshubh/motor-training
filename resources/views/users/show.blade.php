@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>All Users</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('users.index')}}">Users</a></li>
              <li class="active">{{$users->name}}</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Profile
          </div>
          <div class="card-body">
            <div class="row">
              <div class ="col-sm-12 col-md-3 col-lg-3">
                @if(!empty(Auth::user()->profile_pic))
                <img alt = "profile picture" src = "/storage/profile-pics/{{$users->profile_pic}}" class="img-circle img-responsive" />
                @else
                <img src="{{ url('assets/images/avatar-3.jpg')}}" alt="" class="img-circle img-responsive"></a>
                @endif
              </div>
              <div class ="col-sm-12 col-md-9 col-lg-9">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Full Name</td>
                      <td>{{ $users->name }}</td>
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>{{ $users->email }}</td>
                    </tr>
                    <tr>
                      <td>Phone Number</td>
                      <td>{{ $users->phone_number }}</td>
                    </tr>
                    <tr>
                      <td>Gender</td>
                      <td>{{ $users->gender }}</td>
                    </tr>
                    <tr>
                      <td>Birthdate</td>
                      <td>{{ $users->dob }}</td>
                    </tr>
                    <tr>
                      <td>Address</td>
                      <td>{{ $users->address }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@stop
