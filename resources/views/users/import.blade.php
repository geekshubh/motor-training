@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Import User</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('users.index')}}">Users</a></li>
              <li class="active">Import Users</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Import Users
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            <form class="form-horizontal" method="POST" action="{{ route('users.import_fields') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                <label for="csv_file" class="col-md-4 control-label">Select CSV File</label>
                <div class="col-md-6">
                  <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-offset-4 col-md-6 ">
                  <div class="checkbox">
                    <input type="checkbox" name="header" id="md_checkbox_1" class="filled-in chk-col-red" checked />
                    <label for="md_checkbox_1">&nbsp;&nbsp;&nbsp;Does File Contain Header row</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">Parse CSV</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@endsection
