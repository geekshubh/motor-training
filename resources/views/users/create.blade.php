@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Add User</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('users.index')}}">Users</a></li>
              <li class="active">Add User</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Add User
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">@lang('translate.back_to_list')</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::open(['method' => 'POST', 'route' => ['users.store'] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Full Name</h6>
              {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('name'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Roll No.</h6>
              {!! Form::text('roll_no', old('roll_no'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('roll_no'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('roll_no') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Email Address</h6>
              {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('email'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('email') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Password</h6>
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('password'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('password') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Phone Number</h6>
              {!! Form::text('phone_number',old('phone_number'),['class'=>'form-control']) !!}
              @if($errors->has('phone_number'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('phone_number') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Gender</h6>
              {!! Form::select('gender', $gender, old('gender'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Birthdate</h6>
              <div class="input-group date" id="users-create-dob" data-target-input="nearest">
                {!! Form::text('dob',old('dob'),['class' =>'form-control datetimepicker-input','data-target'=>'#users-create-dob']) !!}
                <div class="input-group-append" data-target="#users-create-dob" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
              </div>
              @if($errors->has('dob'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('dob') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Address</h6>
              {!! Form::textarea('address',old('address'),['class'=>'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Profile Picture</h6>
              {{ Form::file('profile_pic',['class'=>'form-control']) }}
              @if($errors->has('profile_pic'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('profile_pic') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>User Role</h6>
              {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
              @if($errors->has('role_id'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('role_id') }}</strong>
              </div>
              @endif
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
