@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Import User</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('users.index')}}">Users</a></li>
              <li class="active">Import Users</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Import Users
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">Back to List</a>
          </div>
          <div class="card-body">
            <br>
            <form class="form-horizontal" method="POST" action="{{ route('users.import_process') }}">
              {{ csrf_field() }}
              <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}" />
              <table class="table table-bordered table-striped table-hover table-responsive table-body">
                @if (isset($csv_header_fields))
                <tr>
                  @foreach ($csv_header_fields as $csv_header_field)
                  <th>{{ $csv_header_field }}</th>
                  @endforeach
                </tr>
                @endif
                @foreach ($csv_data as $row)
                <tr>
                  @foreach ($row as $key => $value)
                  <td>{{ $value }}</td>
                  @endforeach
                </tr>
                @endforeach
                <tr>
                  @foreach ($csv_data[0] as $key => $value)
                  <td>
                    <select name="fields[{{ $key }}]">
                    @foreach (config('import.users_db_fields') as $db_field)
                    <option value="{{ (\Request::has('header')) ? $db_field : $loop->index }}" @if ($key === $db_field) selected @endif>{{ $db_field }}</option>
                    @endforeach
                    </select>
                  </td>
                  @endforeach
                </tr>
              </table>
              <br>
              <button type="submit" class="btn btn-success">Import</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@endsection
