@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>View Fee</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('fees.index')}}">Fees</a></li>
              <li class="active">View Fees</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">View Fee
            <a href="{{ route('fees.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <div class="row">
              <div class ="col-sm-12 col-md-12 col-lg-12">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>Client</td>
                      <td>{{ $fees->fees->name }}</td>
                    </tr>
                    <tr>
                      <td>Amount</td>
                      <td>{{ $fees->amount }}</td>
                    </tr>
                    <tr>
                      <td>Tax</td>
                      <td>{{ $fees->tax }}</td>
                    </tr>
                    <tr>
                      <td>Discount</td>
                      <td>{{ $fees->discount }}</td>
                    </tr>
                    <tr>
                      <td>Total Amount</td>
                      <td>{{ $fees->total_amount }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@stop
