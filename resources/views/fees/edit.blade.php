@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Edit Fee</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('fees.index')}}">Fees</a></li>
              <li class="active">Edit Fee</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Edit Fee
            <a href="{{ route('fees.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::model($fees,['method' => 'PUT', 'route' => ['fees.update',$fees->id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Client</h6>
              {!! Form::select('client_id', $clients, old('client_id'), ['class' => 'standardSelect','tabindex'=>'1']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Amount</h6>
              {!! Form::text('amount', old('amount'), ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Tax</h6>
              {!! Form::text('tax', old('tax'), ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Discount</h6>
              {!! Form::text('discount', old('discount'), ['class' => 'form-control']) !!}
            </div>
            <br>
            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
