@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Edit Review</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li><a href="{{ route('reviews.index')}}">Reviews</a></li>
              <li class="active">Edit Review</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Edit Review
            <a href="{{ route('reviews.index') }}" class="btn btn-default btn-danger float-right">Back To List</a>
          </div>
          <div class="card-body">
            <br>
            {!! Form::model($reviews,['method' => 'PUT', 'route' => ['reviews.update',$reviews->id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Title</h6>
              {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>Description</h6>
              {!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop
