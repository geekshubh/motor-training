@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Reviews</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
              <li class="active">Reviews</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Reviews</strong>
            @if(Auth::user()->isAdmin())
              <a href="{{ route('reviews.create')}}" class="btn btn-md btn-success float-right">Add Vehicle</a>
            @endif
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if (count($reviews) > 0)
                @foreach ($reviews as $reviews)
                <tr>
                  <td>{{ $reviews->title }}</td>
                  <td>
                    @if(Auth::user()->isAdmin())
                    <a href="{{ route('reviews.edit',[$reviews->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                    @endif
                    @if(Auth::user()->isStudent())
                    <a href="{{ route('review-submission.create',[$reviews->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Submit Review</a>
                    @endif
                  </td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="7">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- .content -->
@include('partials.javascripts')
@include('partials.datatablejs')
@stop
