<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/js/tempusdominus-bootstrap-4.min.js" integrity="sha256-z0oKYg6xiLq3yJGsp/LsY9XykbweQlHl42jHv2XTBz4=" crossorigin="anonymous"></script>
<script>
jQuery(function(){
  jQuery('#users-create-dob').datetimepicker({
    format: 'Y-m-d'
  });
});
jQuery(function(){
  jQuery('#car-schedule-create-start-time').datetimepicker({
    format: 'LT'
  });
});
jQuery(function(){
  jQuery('#car-schedule-create-end-time').datetimepicker({
    format: 'LT'
  });
});
</script>
