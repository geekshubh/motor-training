@inject('request', 'Illuminate\Http\Request')
<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        @if(Auth::user()->isAdmin())
        <li class="active">
          <a href="{{ route('dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
        </li>
        <li>
          <a href="{{ route('cars.index')}}"><i class="menu-icon fa fa-car" aria-hidden="true"></i> Cars</a>
        </li>
        <li>
          <a href="{{ route('fees.index')}}"><i class="menu-icon fa fa-money" aria-hidden="true"></i> Fees</a>
        </li>
        <li>
          <a href="{{ route('car-instructor-schedule.index')}}"><i class="menu-icon fa fa-calendar-plus-o" aria-hidden="true"></i> Car Schedule</a>
        </li>
        <li>
          <a href="{{ route('client-schedule.index')}}"><i class="menu-icon fa fa-calendar" aria-hidden="true"></i>Schedule</a>
        </li>
        <li>
          <a href="{{ route('users.index')}}"><i class="menu-icon fa fa-users" aria-hidden="true"></i>Users</a>
        </li>
      @endif
      @if(Auth::user()->isInstructor())
      <li class="active">
        <a href="{{ route('dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
      </li>
      <li>
        <a href="{{ route('client-schedule.index')}}"><i class="menu-icon fa fa-calendar" aria-hidden="true"></i>Schedule</a>
      </li>
      @endif
      @if(Auth::user()->isStudent())
      <li class="active">
        <a href="{{ route('dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
      </li>
      <li>
        <a href="{{ route('fees.index')}}"><i class="menu-icon fa fa-money" aria-hidden="true"></i> Fees</a>
      </li>
      <li>
        <a href="{{ route('car-instructor-schedule.index')}}"><i class="menu-icon fa fa-calendar-plus-o" aria-hidden="true"></i> Car Schedule</a>
      </li>
      <li>
        <a href="{{ route('client-schedule.index')}}"><i class="menu-icon fa fa-calendar" aria-hidden="true"></i>Schedule</a>
      </li>
      @endif
    </ul>
    </div>
    <!-- /.navbar-collapse -->
  </nav>
</aside>
