<script>
CKEDITOR.replace('question_text',{
    extraPlugins:'mathjax',
    mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
CKEDITOR.replace('option_1',{
  extraPlugins:'mathjax',
  mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
CKEDITOR.replace('option_2',{
  extraPlugins:'mathjax',
  mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
CKEDITOR.replace('option_3',{
  extraPlugins:'mathjax',
  mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
CKEDITOR.replace('option_4',{
  extraPlugins:'mathjax',
  mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
CKEDITOR.replace('answer_explanation',{
  extraPlugins:'mathjax',
  mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
});
</script>
