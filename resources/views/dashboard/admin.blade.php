<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
  <div class="row">
    <div class="col-md-6">
      <div class="table-responsive">
        <table id="home-assignment-table" class="table table-striped table-bordered second" style="width:100%">
          <thead class="bg-light text-capitalize">
            <h3>Assignments</h3>
            <tr>
              <th>@lang('translate.title')</th>
              <th>@lang('translate.subject')</th>
              <th>@lang('translate.assignment.deadline')</th>
              <th>@lang('translate.options')</th>
            </tr>
          </thead>
          <tbody>
            @if (count($assignments) > 0)
            @foreach ($assignments as $assignment)
            <tr>
              <td>{{ $assignment->assignment_subjects->title }}</td>
              <td>{{ $assignment->title }}</td>
              <td>{{ $assignment->deadline }}</td>
              <td>
                <a href="{{ route('assignments.show',[$assignment->id]) }}" class="btn btn-xs btn-primary">@lang('translate.assignment.view_assignment')</a>
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-6">
      <div class="table-responsive">
        <table id="home-attendance-table" class="table table-striped table-bordered second" style="width:100%">
          <thead class="bg-light text-capitalize">
            <h3>Classrooms</h3>
            <tr>
              <th>@lang('translate.title')</th>
              <th>@lang('translate.options')</th>
            </tr>
          </thead>
          <tbody>
            @if (count($classrooms) > 0)
            @foreach ($classrooms as $classroom)
            <tr>
              <td>{{ $classroom->title }}</td>
              <td>
                <a href="{{ route('classroom.show',[$classroom->id]) }}" class="btn btn-xs btn-primary">@lang('translate.classrooms.view_classroom')</a>
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <br>
  <hr>
  <br>
  <div class="row">
    <div class="col-md-6">
      <div class="table-responsive">
        <table id="home-material-table" class="table table-striped table-bordered second" style="width:100%">
          <thead class="bg-light text-capitalize">
            <h3>Study Materials</h3>
            <tr>
              <th>@lang('translate.title')</th>
              <th>@lang('translate.teacher_name')</th>
              <th>@lang('translate.options')</th>
            </tr>
          </thead>
          <tbody>
            @if (count($materials) > 0)
            @foreach ($materials as $material)
            <tr>
              <td>{{ $material->title }}</td>
              <td>{{ $material->material_teacher_name->name }}</td>
              <td>
                @if (!empty($material->file_url))
                <a href="/storage/materials/{{ $material->file_url }}" class="btn btn-xs btn-primary">@lang('translate.materials.download_material')</a>
                @elseif (!empty($material->online_url))
                <a href="{{ $material->online_url}}" class="btn btn-xs btn-primary">@lang('translate.materials.download_material')</a>
                @else
                <a href="{{ route('materials.index') }}" class="btn btn-xs btn-primary">@lang('translate.Material')</a>
                @endif
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-6">
      <div class="table-responsive">
        <table id="home-library-books-table" class="table table-striped table-bordered second" style="width:100%">
          <thead class="bg-light text-capitalize">
            <h3>Issued Books</h3>
            <tr>
              <th>@lang('translate.title')</th>
              <th>@lang('translate.library-books.due_date')</th>
              <th>@lang('translate.options')</th>
            </tr>
          </thead>
          <tbody>
            @if (count($library_books) > 0)
            @foreach ($library_books as $book)
            <tr>
              <td>{{ $book->library_books->title }}</td>
              <td>{{ $book->due_date }}</td>
              <td>
                <a href="{{ route('library.show',[$book->book_id])}}" class="btn btn-xs btn-primary">@lang('translate.library.view_book')</a>
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <br>
  <hr>
  <br>
</div>
