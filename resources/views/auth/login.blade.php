@extends('layouts.auth')
@section('content')
<div class="sufee-login d-flex align-content-center flex-wrap">
  <div class="container">
    <div class="login-content">
      <div class="login-logo">
        <a href="index.html">
          <img class="align-content" src="images/logo.png" alt="">
        </a>
      </div>
      <div class="login-form">
        <form class="login-form" id="sign_in" role="form" method="POST" action="{{ url('login') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label>Email address</label>
            <input type="email" class="form-control" placeholder="Email" name="email">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" placeholder="Password" name="password">
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember"> Remember Me
            </label>
          </div>
          <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
