<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ReviewSubmission extends Model
{
  use SoftDeletes;
  protected $table = "review_submission";
  protected $fillable = ['client_id','review_id','instructor_id','question','answer'];

  public function submission_client()
  {
    return $this->belongsTo('App\User','client_id');
  }

  public function submission_instructor()
  {
    return $this->belongsTo('App\User','instructor_id');
  }
}
