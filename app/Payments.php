<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
  protected $table = "payments";
  protected $fillable = ['mid','txn_id','order_id','bank_txn_id','txn_amount','status','resp_code','resp_msg','txn_date','gateway_name','payment_mode','checksum_hash'];
}
