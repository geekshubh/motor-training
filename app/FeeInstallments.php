<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class FeeInstallments extends Model
{
  use SoftDeletes;
  protected $fillable = ['fee_id','client_id','amount','title','installment_unique_id','status','payment_type'];
  protected $table = "fee_installments";

  public function fee_installments()
  {
    return $this->belongsTo('App\User','client_id');
  }
}
