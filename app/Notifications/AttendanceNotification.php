<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class AttendanceNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $info;
    
    public function __construct($attendance,$attendance_status,$subject,$username)
    {
       $status = [ 'none', 'present', 'absent', 'late'];
        $this->info = 'Your ward '.$username.' was '. $status[$attendance_status->status].' on '.$attendance->date.' in '.$subject->title.' lecture';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', TwilioChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->line('Daily Attendance')
                    //->action('Notification Action', url('/'))
                    ->line($this->info);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->info);
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
