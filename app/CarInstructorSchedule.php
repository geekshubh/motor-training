<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarInstructorSchedule extends Model
{
  use SoftDeletes;
  protected $fillable = ['car_id','instructor_id','start_time','end_time','availability','booked_by'];
  protected $table = "car_instructor_schedule";

  public function car_schedule()
  {
    return $this->belongsTo('App\Cars','car_id');
  }
}
