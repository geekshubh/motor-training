<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Fees extends Model
{
  use SoftDeletes;
  protected $fillable = ['client_id','amount','tax','discount','total_amount'];
  protected $table = "fees";

  public function fees()
  {
    return $this->belongsTo('App\User','client_id');
  }
}
