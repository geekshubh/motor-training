<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cars extends Model
{
  use SoftDeletes;
  protected $fillable = ['name','unavailable_on','instructor_id'];
  protected $table = "cars";

  public function client_schedule_car()
  {
    return $this->hasMany('App\ClientSchedule','car_id')->withTrashed();
  }

  public function car_schedule()
  {
    return $this->hasMany('App\CarInstructorSchedule','car_id')->withTrashed();
  }
}
