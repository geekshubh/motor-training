<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Hash;
use Mail;

class User extends Authenticatable implements JWTSubject
{
  use SoftDeletes,Notifiable;
  protected $fillable = ['name', 'email', 'password','remember_token', 'role_id','student_classroom_id','phone_number','address','gender','dob','roll_no','profile_pic'];

  public static function boot()
  {
    parent::boot();
    User::observe(new \App\Observers\UserActionsObserver);
  }

  public function setPasswordAttribute($input)
  {
    if ($input)
    {
      $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
  }

  public function setRoleIdAttribute($input)
  {
    $this->attributes['role_id'] = $input ? $input : null;
  }

  public function role()
  {
    return $this->belongsTo('App\Role', 'role_id')->withTrashed();
  }

  public function client_schedule()
  {
    return $this->hasMany('App\ClientSchedule','client_id')->withTrashed();
  }

  public function instructor_schedule()
  {
    return $this->hasMany('App\ClientSchedule','instructor_id')->withTrashed();
  }

  public function fees()
  {
    return $this->hasMany('App\Fees','client_id');
  }

  public function fee_installments()
  {
    return $this->hasMany('App\FeeInstallments','client_id')->withTrashed();
  }

  public function submission_client()
  {
    return $this->hasMany('App\ReviewSubmission','client_id');
  }

  public function submission_instructor()
  {
    return $this->hasMany('App\ReviewSubmission','instructor_id');
  }

  public function student_name()
  {
    return $this->hasMany('App\Fees', 'student_id')->withTrashed();
  }

  public function isAdmin()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 1)
      {
        return true;
      }
    }
    return false;
  }

  public function isStudent()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 3)
      {
        return true;
      }
    }
    return false;
  }

  public function isInstructor()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 2)
      {
        return true;
      }
    }
    return false;
  }



  protected $hidden =
  [
    'password', 'remember_token',
  ];

  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
    return [];
  }
}
