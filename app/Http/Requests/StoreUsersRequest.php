<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUsersRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return
    [
      'name'          => 'required',
      'email'         => 'required|email|unique:users,email',
      'password'      => 'required',
      'profile_pic'   => 'max:10000|mimes:png,jpeg',
      'role_id'       => 'required',
    ];
  }

  public function messages()
  {
    return
    [
      'name.required'         => trans('validation.full_name'),
      'email.required'        => trans('validation.email'),
      'email.unique'          => trans('validation.email_unique'),
      'role_id.required'      => trans('validation.role_id')
    ];
  }
}
