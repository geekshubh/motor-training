<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }
  public function rules()
  {
    return
    [
      'name'          => 'required',
      'profile_pic'   => 'max:10000|mimes:png,jpeg',
    ];
  }

  public function messages()
  {
    return
    [
      'name.required'           =>  trans('validation.name'),
      'email.required'          =>  trans('validation.email'),
      'profile_pic.max'         =>  trans('validation.profile_pic_max_size'),
      'profile_pic.mimes'       =>  trans('validation.profile_pic_file_type'),
    ];
  }
}
