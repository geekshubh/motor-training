<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      return view('home');
    }
    else
    {
      return view('home');
    }
  }
}
