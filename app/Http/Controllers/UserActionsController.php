<?php

namespace App\Http\Controllers;

use App\UserAction;
use Illuminate\Http\Request;
use Auth;
class UserActionsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $user_actions = UserAction::all();
      return view('user_actions.index', compact('user_actions'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('Controller.user-actions.view_error'));
    }
  }
}
