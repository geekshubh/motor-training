<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cars;
use Auth;
class CarsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::all();
      return view('cars.index',compact('cars'));
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $days = ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
      $instructors = User::where('role_id',2)->pluck('name','id');
      return view('cars.create',compact('days','instructors'));
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = New Cars;
      $cars->name = $request->name;
      $cars->unavailable_on = $request->unavailable_on;
      $cars->instructor_id = $request->instructor_id;
      $cars->save();
      return redirect()->route('cars.index')->with('success','Car Added Successfully');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $days = ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
      $instructors = User::where('role_id',2)->pluck('name','id');
      return view('cars.edit',compact('cars','days','instructors'));
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $cars->name = $request->name;
      $cars->unavailable_on = $request->unavailable_on;
      $cars->instructor_id = $request->instructor_id;
      $cars->update();
      return redirect()->route('cars.index')->with('success','Car Updated Successfully');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $cars->delete();
      return redirect()->route('cars.index')->with('success','Car Deleted Successfully');
    }
  }
}
