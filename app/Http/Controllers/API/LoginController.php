<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
  public function login(Request $request)
  {
    $credentials = $request->only('email', 'password');
    try
    {
      if (! $token = JWTAuth::attempt($credentials))
      {
        return response()->json(['error' => 'invalid_credentials'], 400);
      }
    }
    catch (JWTException $e)
    {
      return response()->json(['error' => 'could_not_create_token'], 500);
    }
    return response()->json(compact('token'));
  }

  public function me()
  {
    if(Auth::user()->isAdmin())
    {
      return response()->json($this->guard()->user());
    }
  }
}
