<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserProfileRequest;
use Validator;
use JWTFactory;
use JWTAuth;


class ProfileController extends Controller
{
  // This function gets the user and returns the profile page
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $users = Auth::user();
      return response()->json(compact('users'),200);
    }
    else
    {
      return response()->json(['errors' => 'You are not allowed to View the profile'], 401);
    }
  }
  // This function shows the edit page for editing the profile
  public function edit($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $user = User::findOrFail($id);
      $gender = ['Male'=>'Male','Female'=>'Female','Others'=>'Others'];
      return response()->json(compact('user','gender'),200);
    }
    else
    {
      return response()->json(['errors' => 'You are not allowed to edit the profile'], 500);
    }
  }

  // Update the user data in the table
  public function update(UpdateUserProfileRequest $request, $id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      if($request->hasFile('profile_pic'))
      {
        // Get filename with the extension
        $filenameWithExt = $request->file('profile_pic')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('profile_pic')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore= $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $request->file('profile_pic')->storeAs('public/profile-pics', $fileNameToStore);
      }
      else
      {
        $fileNameToStore = '';
      }
      $users = User::findOrFail($id);
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->phone_number = $request->input('phone_number');
      $users->address = $request->input('address');
      $users->gender = $request->input('gender');
      $users->dob = $request->input('dob');
      if($request->hasFile('profile_pic'))
      {
        $users->profile_pic = $fileNameToStore;
      }
      $users->save();
      return response()->json(['success' => 'Profile has been successfully updated'], 200);
    }
    else
    {
      return response()->json(['errors' => 'You are not allowed to update the profile'], 500);
    }
  }
}
