<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CarInstructorSchedule;
use App\Cars;
use Auth;
use App\ClientSchedule;
use Carbon\Carbon;
class ClientScheduleController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $currentDate = Carbon::now();
      $startDate = $currentDate->copy()->startOfWeek()->format('Y-m-d');
      $endDate = $currentDate->copy()->endOfWeek()->format('Y-m-d');
      $schedule = ClientSchedule::whereBetween('date',[$startDate,$endDate])->get();
      return view('client-schedule.index',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $currentDate = Carbon::now();
      $startDate = $currentDate->copy()->startOfWeek()->format('Y-m-d');
      $endDate = $currentDate->copy()->endOfWeek()->format('Y-m-d');
      //dd($startDate);
      $schedule = ClientSchedule::where('client_id',Auth::id())->whereBetween('date',[$startDate,$endDate])->get();
      return view('client-schedule.index',compact('schedule'));
    }
    elseif(Auth::user()->isInstructor())
    {
      $schedule = ClientSchedule::where('instructor_id',Auth::id())->get();
      return view('client-schedule.index',compact('schedule'));
    }
  }

  public function book_appointment($schedule_id)
  {
    if(Auth::user()->isStudent())
    {
      $schedule = CarInstructorSchedule::where('id',$schedule_id)->first();
      $start_time = $schedule->start_time;
      $end_time = $schedule->end_time;
      $instructor_id = $schedule->instructor_id;
      $car_id = $schedule->car_id;
      $car_day_off = Cars::where('id',$car_id)->pluck('unavailable_on')->first();
      $date = Carbon::now();
      $i = 0;
      while($i < 23)
      {
        $new_date = $date->addDays(1);
        $formatted_date = $new_date->format('Y-m-d');
        $check_day = $new_date->format('l');
        if($check_day != $car_day_off)
        {
          $client_schedule = new ClientSchedule;
          $client_schedule->schedule_id = $schedule_id;
          $client_schedule->car_id = $car_id;
          $client_schedule->instructor_id = $instructor_id;
          $client_schedule->start_time = $start_time;
          $client_schedule->end_time = $end_time;
          $client_schedule->date = $formatted_date;
          $client_schedule->client_id = Auth::id();
          $client_schedule->save();
          $i++;
        }
      }
      $schedule = CarInstructorSchedule::findOrFail($schedule_id);
      $schedule->booked_by = Auth::id();
      $schedule->availability = "Unavailable";
      $schedule->update();
      return redirect()->route('client-schedule.index');
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_schedule->car_id = $request->car_id;
      $client_schedule->instructor_id = $request->instructor_id;
      $client_schedule->start_time = $request->start_time;
      $client_schedule->end_time = $request->end_time;
      $client_schedule->date = $request->date;
      $client_schedule->client_id = $request->client_id;
      return redirect()->route('client-schedule.index')->with('success','Schedule Updated Successfully');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_schedule->delete();
      return redirect()->route('client-schedule.index')->with('success','Schedule Deleted Successfully');
    }
  }

  public function instructor_login($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $instructor_login = $client_schedule->instructor_login_time;
      if(empty($instructor_login))
      {
        $client_schedule->instructor_login_time = Carbon::now()->format('d-m-Y H:i');
        $client_schedule->update();
        return redirect()->route('client-schedule.index')->with('success','Instructor Logged in Successfully');
      }
      else
      {
        return redirect()->back()->with('warning','Instructor Logged In Already');
      }
    }
  }

  public function student_login($id)
  {
    if(Auth::user()->isStudent())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_login = $client_schedule->client_login_time;
      $instructor_login = $client_schedule->instructor_login_time;
      if(!empty($instructor_login) && empty($client_login))
      {
        $client_schedule->client_login_time = Carbon::now()->format('d-m-Y H:i');
        $client_schedule->update();
        return redirect()->route('client-schedule.index')->with('success','Student Logged in Successfully');
      }
      elseif(empty($instructor_login))
      {
        return redirect()->back()->with('warning','Instructor has not Logged In');
      }
      elseif(!empty($client_login))
      {
        return redirect()->back()->with('warning','You have already logged in');
      }
    }
  }

  public function instructor_logout($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $instructor_login = $client_schedule->instructor_login_time;
      $client_login = $client_schedule->client_login_time;
      $client_logout = $client_schedule->client_logout_time;
      $instructor_logout = $client_schedule->instructor_logout_time;
      if(!empty($instructor_login) && !empty($client_login) && !empty($client_logout) && empty($instructor_logout))
      {
        $client_schedule->instructor_logout_time = Carbon::now()->format('d-m-Y H:i');
        $instructor_login_time = Carbon::parse($instructor_login);
        $instructor_logout_time = Carbon::parse($client_schedule->instructor_logout_time);
        $difference = $instructor_logout_time->diffInMinutes($instructor_login_time);
        $client_schedule->instructor_total_time = $difference;
        $average_time = (($difference + $client_schedule->client_total_time)/2);
        $client_schedule->average_time = $average_time;
        $client_schedule->update();
        return redirect()->route('client-schedule.index')->with('success','Instructor Logged Out Successfully');
      }
      elseif(!empty($instructor_login) && !empty($client_login) && empty($client_logout))
      {
        return redirect()->back()->with('warning','Client has not logged out yet');
      }
      else
      {
        return redirect()->back()->with('warning','Client/Instructor not logged in Yet!');
      }
    }
  }

  public function student_logout($id)
  {
    if(Auth::user()->isStudent())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_login = $client_schedule->client_login_time;
      $instructor_login = $client_schedule->instructor_login_time;
      $client_logout = $client_schedule->client_logout_time;
      if(!empty($client_login) && !empty($instructor_login) && empty($client_logout))
      {
        $client_schedule->client_logout_time = Carbon::now()->format('d-m-Y H:i');
        $client_login_time = Carbon::parse($client_login);
        $client_logout_time = Carbon::parse($client_schedule->client_logout_time);
        $diff = $client_logout_time->diffInMinutes($client_login_time);
        $client_schedule->client_total_time = $diff;
        $client_schedule->update();
        return redirect()->route('client-schedule.index')->with('success','Student Logged out successfully');
      }
      elseif(!empty($client_logout))
      {
        return redirect()->back()->with('warning','You have logged out already');
      }
      else
      {
        return redirect()->back()->with('warning','Client/Instructor not logged in');
      }
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = ClientSchedule::findOrFail($id);
      if(empty($schedule->average_time))
      {
        return view('client-schedule.edit',compact('schedule'));
      }
      else
      {
        return redirect()->back()->with('warning','Schedule already completed');
      }
    }
  }

  public function show($id)
  {
    $schedule = ClientSchedule::findOrFail($id);
    if(Auth::user()->isAdmin())
    {
      return view('client-schedule.show',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $student_id = $schedule->client_id;
      if(Auth::id() == $student_id)
      {
        return view('client-schedule.show',compact('schedule'));
      }
    }
    elseif(Auth::user()->isInstructor())
    {
      $instructor_id = $schedule->instructor_id;
      if(Auth::id() == $instructor_id)
      {
        return view('client-schedule.show',compact('schedule'));
      }
    }
  }
}
