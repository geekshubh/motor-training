<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reviews;
use Auth;
use App\ReviewSubmission;
use App\ClientSchedule;
class ReviewSubmissionController extends Controller
{
  public function index($review_id)
  {
    if(Auth::user()->isAdmin())
    {
      $review_submission = ReviewSubmission::where('review_id',$review_id)->get();
      return view('review-submission.index',compact('review_submission'));
    }
    elseif(Auth::user()->isStudent())
    {
      $review_submission = ReviewSubmission::where([['review_id',$review_id],['client_id',Auth::id()]])->get();
      return view('review-submission.index',compact('review_submission'));
    }
  }

  public function create($review_id)
  {
    if(Auth::user()->isStudent())
    {
      return view('review-submission.create',compact('review_id'));
    }
  }

  public function store(Request $request,$review_id)
  {
    if(Auth::user()->isStudent())
    {
      $instructor_id = ClientSchedule::where('client_id',Auth::id())->pluck('instructor_id')->first();
      //dd($request->answers);
      foreach ($request->input('questions', []) as $key => $question)
      {
        $review_submission = new ReviewSubmission;
        $review_submission->review_id = $review_id;
        $review_submission->client_id = Auth::id();
        $review_submission->instructor_id = $instructor_id;
        $review_submission->question = $question;
        $review_submission->answer = $request->input('answers.'.$question);
        $review_submission->save();
        return redirect()->route('reviews.index');
      }
    }
  }

  public function show($review_id,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $review_submission = ReviewSubmission::findOrFail($id);
      return view('review-submission.show',compact('review_submission'));
    }
  }
}
