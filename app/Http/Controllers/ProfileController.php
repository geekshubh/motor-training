<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserProfileRequest;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
  // This function gets the user and returns the profile page
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $user = Auth::user();
      $profile_pic = $user->profile_pic;
      if ($profile_pic != '' || $profile_pic != null) {
        $image = Storage::disk('s3')->temporaryUrl($profile_pic,now()->addSeconds(5));
      }
      else {
        $image = null;
      }
      return view('users.profile', compact('user','image'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.view_error'));
    }
  }
  // This function shows the edit page for editing the profile
  public function edit($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $user = User::findOrFail($id);
      $gender = ['Male'=>'Male','Female'=>'Female','Others'=>'Others'];
      return view('users.edit_profile', compact('user','gender'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.edit_error'));
    }
  }

  // Update the user data in the table
  public function update(UpdateUserProfileRequest $request, $id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      if($request->hasFile('profile_pic'))
      {
        // Get filename with the extension
        $filenameWithExt = $request->file('profile_pic')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('profile_pic')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore= $filename.'_'.time().'.'.$extension;
        $filePath = 'profile_pic/' . $fileNameToStore;
        // Upload Image
        $file = $request->file('profile_pic');
        //$path = $request->file('profile_pic')->storeAs('public/profile-pics', $fileNameToStore);
        Storage::disk('s3')->put($filePath, file_get_contents($file));
           
           
      }
      else
      {
        $fileNameToStore = '';
      }
      $users = User::findOrFail($id);
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->phone_number = $request->input('phone_number');
      $users->address = $request->input('address');
      $users->gender = $request->input('gender');
      $users->dob = $request->input('dob');
      if($request->hasFile('profile_pic'))
      {
        $users->profile_pic = $filePath;
      }
      $users->save();
      return redirect()->route('users.profile')->with('success', 'Profile Updated');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.edit_error'));
    }
  }

  public static function getProfilePic() {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
        {
          $user = Auth::user();
          $profile_pic = $user->profile_pic;
          if ($profile_pic != '' || $profile_pic != null) {
            $image = Storage::disk('s3')->temporaryUrl($profile_pic,now()->addSeconds(5));
          }
          else {
            $image = "/assets/images/avatar-3.jpg";
          }
          return $image;
        }
      }
  }
