<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fees;
use App\User;
use Auth;
class FeesController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::all();
      return view('fees.index',compact('fees'));
    }
    elseif(Auth::user()->isStudent())
    {
      $fees = Fees::where('client_id',Auth::id())->get();
      return view('fees.index',compact('fees'));
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $clients = User::where('role_id',3)->pluck('name','id');
      return view('fees.create',compact('clients'));
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = new Fees;
      $fees->client_id = $request->client_id;
      $fees->amount = $request->amount;
      $fees->tax = $request->tax;
      $fees->discount = $request->discount;
      if(!empty($request->discount))
      {
        $discounted_amount = $fees->amount - ($fees->amount *($request->discount/100));
      }
      if(!empty($request->tax) && !empty($discounted_amount))
      {
        $tax = $discounted_amount * ($request->tax/100);
        $fees->total_amount = $discounted_amount + $tax;
      }
      elseif(!empty($request->tax) && empty($discounted_amount))
      {
        $fees->total_amount = $fees->amount + ($fees->amount * ($request->tax/100));
      }
      elseif(empty($request->tax) && !empty($discounted_amount))
      {
        $fees->total_amount = $discounted_amount;
      }
      else
      {
        $fees->total_amount = $fees->amount;
      }
      $fees->save();
      return redirect()->route('fees.index')->with('success','Fees Added Successfully');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $clients = User::where('role_id',3)->pluck('name','id');
      return view('fees.edit',compact('fees','clients'));
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $fees->client_id = $request->client_id;
      $fees->amount = $request->amount;
      $fees->tax = $request->tax;
      $fees->discount = $request->discount;
      if(!empty($request->discount))
      {
        $discounted_amount = $fees->amount - ($fees->amount *($request->discount/100));
      }
      if(!empty($request->tax) && !empty($discounted_amount))
      {
        $tax = $discounted_amount * ($request->tax/100);
        $fees->total_amount = $discounted_amount + $tax;
      }
      elseif(!empty($request->tax) && empty($discounted_amount))
      {
        $fees->total_amount = $fees->amount + ($fees->amount * ($request->tax/100));
      }
      elseif(empty($request->tax) && !empty($discounted_amount))
      {
        $fees->total_amount = $discounted_amount;
      }
      else
      {
        $fees->total_amount = $fees->amount;
      }
      $fees->update();
      return redirect()->route('fees.index')->with('success','Fees Added Successfully');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $fees->delete();
      return redirect()->route('fees.index')->with('success','Fees Added Successfully');
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      return view('fees.show',compact('fees'));
    }
  }
}
