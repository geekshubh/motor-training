<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CarInstructorSchedule;
use App\Cars;
use Auth;
class CarInstructorScheduleController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarInstructorSchedule::distinct('car_id')->get();
      //dd($schedule);
      return view('car-instructor-schedule.index',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $check_if_exists = CarInstructorSchedule::where('booked_by',Auth::id())->count();
      //dd($check_if_exists);
      if($check_if_exists == 0)
      {
        $schedule = CarInstructorSchedule::distinct('car_id')->get();
        return view('car-instructor-schedule.index',compact('schedule'));
      }
      else
      {
        return redirect()->route('client-schedule.index');
      }
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::all()->pluck('name','id');
      //$instructors = User::where('role_id',2)->pluck('name','id');
      $availability = ['Available'=>'Available','Unavailable'=>'Unavailable'];
      return view('car-instructor-schedule.create',compact('cars','availability'));
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = New CarInstructorSchedule;
      $schedule->car_id = $request->car_id;
      $instructor_id = Cars::where('id',$request->car_id)->pluck('instructor_id')->first();
      $schedule->instructor_id = $instructor_id;
      $schedule->start_time = $request->start_time;
      $schedule->end_time = $request->end_time;
      $schedule->availability = $request->availability;
      $schedule->save();
      return redirect()->route('car-instructor-schedule.index')->with('success','Schedule Set up Successfully');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarInstructorSchedule::findOrFail($id);
      $cars = Cars::all()->pluck('title','id');
      //$instructors = User::where('role_id',2)->pluck('name','id');
      $availability = ['Available'=>'Available','Unavailable'=>'Unavailable'];
      return view('car-instructor-schedule.edit',compact('cars','schedule','availability'));
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarInstructorSchedule::findOrFail($id);
      $schedule->car_id = $request->car_id;
      $instructor_id = Cars::where('id',$request->car_id)->pluck('instructor_id')->first();
      $schedule->instructor_id = $instructor_id;
      $schedule->start_time = $request->start_time;
      $schedule->end_time = $request->end_time;
      $schedule->booked_by = $request->booked_by;
      $schedule->availability = $request->availability;
      $schedule->update();
      return redirect()->route('car-instructor-schedule.index')->with('success','Schedule Updated Successfully');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarInstructorSchedule::findOrFail($id);
      $schedule->delete();
      return redirect()->route('car-instructor-schedule.index')->with('success','Schedule Deleted Successfully');
    }
  }

  public function show($car_id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarInstructorSchedule::where('car_id',$car_id)->get();
      return view('car-instructor-schedule.view',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $check_if_exists = CarInstructorSchedule::where('booked_by',Auth::id())->count();
      //dd($check_if_exists);
      if($check_if_exists == 0)
      {
        $schedule = CarInstructorSchedule::where('car_id',$car_id)->get();
        return view('car-instructor-schedule.view',compact('schedule'));
      }
      else
      {
        return redirect()->back();
      }
    }
  }
}
