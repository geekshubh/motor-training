<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fees;
use App\User;
use App\FeeInstallments;
use Illuminate\Support\Str;
use Auth;

class FeeInstallmentsController extends Controller
{
  public function index($fee_id)
  {
    if(Auth::user()->isAdmin())
    {
      $installments = FeeInstallments::where('fee_id',$fee_id)->get();
      return view('fee-installments.index',compact('installments','fee_id'));
    }
    elseif(Auth::user()->isStudent())
    {
      $client_id = FeeInstallments::where('fee_id',$fee_id)->pluck('client_id')->first();
      if($client_id == Auth::id())
      {
        $installments = FeeInstallments::where('fee_id',$fee_id)->get();
        return view('fee-installments.index',compact('installments','fee_id'));
      }
    }
  }

  public function create($fee_id)
  {
    if(Auth::user()->isAdmin())
    {
      $status = ['Paid'=>'Paid','Unpaid'=>'Unpaid'];
      $type = ['Online'=>'Online','Offline'=>'Offline'];
      return view('fee-installments.create',compact('fee_id','status','type'));
    }
  }

  public function store(Request $request,$fee_id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_id = Fees::where('id',$fee_id)->pluck('client_id')->first();
      $installment = new FeeInstallments;
      $installment->fee_id = $fee_id;
      $installment->client_id = $client_id;
      $installment->amount = $request->amount;
      $installment->title = $request->title;
      $installment->installment_unique_id =  Str::uuid();
      $installment->payment_type = $request->payment_type;
      $installment->status = $request->status;
      $installment->save();
      return redirect()->route('fee-installments.index',[$fee_id])->with('success','Installment Added Successfully');
    }
  }

  public function edit($fee_id,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $installment = FeeInstallments::findOrFail($id);
      $status = ['Paid'=>'Paid','Unpaid'=>'Unpaid'];
      $type = ['Online'=>'Online','Offline'=>'Offline'];
      return view('fee-installments.edit',compact('installment','fee_id','status','type'));
    }
  }

  public function update(Request $request,$fee_id,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_id = Fees::where('id',$fee_id)->pluck('client_id')->first();
      $installment = FeeInstallments::findOrFail($id);
      $installment->fee_id = $fee_id;
      $installment->client_id = $client_id;
      $installment->amount = $request->amount;
      $installment->title = $request->title;
      $installment->installment_unique_id =  Str::uuid();
      $installment->payment_type = $request->payment_type;
      $installment->status = $request->status;
      $installment->save();
      return redirect()->route('fee-installments.index',[$fee_id])->with('success','Installment Updated Successfully');
    }
  }

  public function destroy($fee_id,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $installment = FeeInstallments::findOrFail($id);
      $installment->delete();
      return redirect()->route('fee-installments.index',[$fee_id])->with('success','Installment Deleted Successfully');
    }
  }

  public function show($fee_id,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $installment = FeeInstallments::findOrFail($id);
      return view('fee-installments.show',compact('installment','fee_id'));
    }
    elseif(Auth::user()->isStudent())
    {
      $client_id = FeeInstallments::where('id',$id)->pluck('client_id')->first();
      if($client_id == Auth::id())
      {
        $installment = FeeInstallments::findOrFail($id);
        return view('fee-installments.show',compact('installment','fee_id'));
      }
    }
  }
}
