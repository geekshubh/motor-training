<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reviews;
use App\ClientSchedule;
class ReviewsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $reviews = Reviews::all();
      return view('reviews.index',compact('reviews'));
    }
    elseif(Auth::user()->isStudent())
    {
      $client_schedule = ClientSchedule::where('client_id',Auth::id())->pluck('average_time')->filter();
      $count = $client_schedule->count();

      //dd($count);
      $reviews = Reviews::where('id',100)->get();
      if(($count > 4) && ($count < 10))
      {
        $reviews = Reviews::where('id',1)->get();
        return view('reviews.index',compact('reviews'));
      }
      elseif(($count >= 10) && ($count < 15))
      {
        $reviews = Reviews::where('id',2)->get();
        return view('reviews.index',compact('reviews'));
      }
      elseif(($count >= 15) && ($count < 20))
      {
        $reviews = Reviews::where('id',3)->get();
        return view('reviews.index',compact('reviews'));
      }
      elseif(($count >= 20))
      {
        $reviews = Reviews::where('id',4)->get();
        return view('reviews.index',compact('reviews'));
      }
      return view('reviews.index',compact('reviews'));
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      return view('reviews.create');
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $reviews = new Reviews;
      $reviews->title = $request->title;
      $reviews->description = $request->description;
      $reviews->save();
      return redirect()->route('reviews.index');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $reviews = Reviews::findOrFail($id);
      return view('reviews.edit',compact('reviews'));
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $reviews->title = $request->title;
      $reviews->description = $request->description;
      $reviews->update();
      return redirect()->route('reviews.index');
    }
  }
}
