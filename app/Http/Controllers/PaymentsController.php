<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PaytmWallet;
use App\FeeInstallments;
use Auth;

class PaymentsController extends Controller
{
  public function order($fee_id,$id)
    {
        $payment = PaytmWallet::with('receive');
        $installment = FeeInstallments::where('id',$id)->first();
        $payment->prepare([
          'order' => $installment->installment_unique_id,
          'user' => Auth::id(),
          'mobile_number' => Auth::user()->phone_number,
          'email' => Auth::user()->email,
          'amount' => $installment->amount,
          'callback_url' => 'https://rkmt.e-ducate.in/payment/status'
        ]);
        return $payment->receive();
    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');
        $response = $transaction->response();
        if($transaction->isSuccessful())
        {
          $installment_unique_id = $transaction->getOrderId();
          $installment = FeeInstallments::where('installment_unique_id',$installment_unique_id);
          $installment->status = 'Paid';
          $installment->update();
          return redirect()->route('fees.index')->with('success','Fees Paid Successfully');
        }
        else if($transaction->isFailed())
        {
          $failure = 'failure';
          dd($failure);
        }
        else if($transaction->isOpen())
        {
          $failure = 'processing';
          dd($failure);
        }
        else
        {
          dd($transaction);
        }
    }
}
