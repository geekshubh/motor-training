<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id')->nullable()->unsigned();
            $table->integer('car_id')->nullable()->unsigned();
            $table->integer('instructor_id')->nullable()->unsigned();
            $table->integer('client_id')->nullable()->unsigned();
            $table->foreign('schedule_id')->references('id')->on('car_instructor_schedule')->onDelete('cascade');
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
            $table->foreign('instructor_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('date')->nullable();
            $table->text('start_time')->nullable();
            $table->text('end_time')->nullable();
            $table->text('instructor_login_time')->nullable();
            $table->text('client_login_time')->nullable();
            $table->text('instructor_logout_time')->nullable();
            $table->text('client_logout_time')->nullable();
            $table->text('instructor_total_time')->nullable();
            $table->text('client_total_time')->nullable();
            $table->text('average_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_schedule');
    }
}
