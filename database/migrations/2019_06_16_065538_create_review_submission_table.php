<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewSubmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_submission', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('review_id')->unsigned();
            $table->integer('instructor_id')->unsigned();
            $table->text('question');
            $table->text('answer');
            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('instructor_id')->references('id')->on('users');
            $table->foreign('review_id')->references('id')->on('reviews');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_submission');
    }
}
