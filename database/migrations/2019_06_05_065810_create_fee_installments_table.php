<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_installments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fee_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('fee_id')->references('id')->on('fees')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('installment_unique_id')->nullable();
            $table->text('title')->nullable();
            $table->text('amount')->nullable();
            $table->text('status')->nullable();
            $table->text('payment_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_installments');
    }
}
