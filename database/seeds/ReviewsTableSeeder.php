<?php

use Illuminate\Database\Seeder;
use App\Reviews;
class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table((new Reviews)->getTable())->truncate();

      Reviews::insert([
          [
              'id'    => 1,
              'title' => 'First Review',
              'description' =>'This Review is taken after the fifth day of training',
          ],
          [
              'id'    => 2,
              'title' => 'Second Review',
              'description'=>'This Review is taken after the tenth day of training',
          ],
          [
              'id'    => 3,
              'title' => 'Third Review',
              'description'=>'This Review is taken after fifteenth day of training',
          ],
          [
              'id'    => 4,
              'title' => 'Fourth Review',
              'description'=>'This Review is taken after last day of training',
          ],
      ]);
    }
}
